# poc-certificat-scolarite

## Maquette FIGMA

https://www.figma.com/file/9qi2Ahf1B6mBNG0Ero5fDn/POC-Datathon?node-id=0%3A1

## Doc DSFR

https://www.systeme-de-design.gouv.fr/elements-d-interface

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
